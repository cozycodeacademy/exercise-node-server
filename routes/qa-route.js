const express = require("express");
const router = express.Router();
const data = require("../db")("qa");
const { auth } = require("../utils/auth");
let current = {};
const MAX_QUESTIONS = 3;

function createProfile(id, gender){
    return { id, gender, questions:[], pass:false };
}

router.post("/start",auth, (req, res)=>{
    const gender = req.body.gender;
    if(!gender || (gender.toLowerCase() !== "m" && gender.toLowerCase() !== "g")){
        return res.json({
            success:false,
            message:"Please provide gender"
        })
    }
    current[req.user.id] = createProfile(req.user.id, gender.toLowerCase());
    
    res.json({
        success:true
    })
})
router.get("/next", auth, (req, res)=>{
    const userId = req.user.id;
    const { gender, questions: usersQuestions } = current[userId];

    if(usersQuestions.length>=MAX_QUESTIONS){
        return res.json({
            success:false,
            message:"Max question answered"
        })
    }

    let questionGender = gender==="m"? "f":"m";
    let nextQuestion;
    let lastQuestion = false;

    do{
        const randQuestionIndex = Math.floor(Math.random() * data[questionGender].length);
        const question = data[questionGender][randQuestionIndex];
        const questionAsked = usersQuestions.find(q=>q.id===question.id);
        if(!questionAsked){
            nextQuestion = {
                ...question, 
                a: question.a.map(a=> a.a)
            };
            current[userId].questions.push(nextQuestion);
            if(current[userId].questions.length===MAX_QUESTIONS){
                lastQuestion = true;
            }
        }
    }while(!nextQuestion)

    res.json({success:true, nextQuestion, lastQuestion});
});
router.post("/ans", auth, (req,res)=>{
    const userId = req.user.id;
    const { questionId, ans } = req.body;

    if(!questionId || !ans){
        return res.json({
            success: false,
            message: "Insufficient info",
        });
    }
    
    let ansLowerCase = ans.toLowerCase();
    if(ansLowerCase !== "a" && ansLowerCase !== "b" && ansLowerCase !== "c"){
        return res.json({
            success: false,
            message: "No such answer",
        });
    }

    const targetQuestion = current[userId].questions.find(q=>q.id===questionId);
    if(!targetQuestion){
        return res.json({
            success:false,
            message:"No such question"
        })
    }
    targetQuestion.answer = ansLowerCase;
    res.json({
        success:true,
        question: targetQuestion
    })
})
router.post("/settlement", auth, (req, res)=>{
    const userId = req.user.id;
    const { gender, questions: usersQuestions } = current[userId];
    if(usersQuestions.length !== MAX_QUESTIONS || usersQuestions.find(q=>!q.answer )){
        return res.json({
            success:false,
            message:"Please finish the questions"
        });
    }

    let questionGender = gender==="m"? "f":"m";

    const point = usersQuestions.reduce((sum, question)=>{
        const {id, answer} = question;
        const srcQuestion = data[questionGender].find(q=>q.id===id);
        console.log(srcQuestion, id, answer, answer.charCodeAt(0)-97)
        sum+=srcQuestion.a[answer.charCodeAt(0)-97].point
        return sum;
    },0)

    if(point>=14){
        current[userId].pass = true;
    }else{
        current[userId].pass = false;
    }

    res.json({
        success:true,
        point,
        pass:current[userId].pass
    })


})


module.exports = router;
