const express = require("express");
const router = express.Router();
const data = require("../db")("user");

router.get("/users", (req, res) => {
  res.json(data);
});

router.post("/users", (req, res) => {
  const name = req.body.name;
  const gender = req.body.gender;

  data.push({
    name,
    gender,
  });

  res.json({
    success: true,
  });
});

router.patch("/users", (req, res) => {
  const name = req.body.name;
  let result = {
    success: true,
  };

  const target = data.find((user) => {
    return user.name === name;
  });

  if (target) {
    target.gender = req.body.gender;
    result.success = true;
  } else {
    result.success = false;
    result.message = "User Not Found";
  }

  res.json(result);
});

router.delete("/users", (req, res)=>{
  const name = req.body.name;
  let result = {
    success: true,
  };

  const userIndex = data.findIndex((user)=>{
    return user.name === name;
  })

  if(userIndex > -1){
    data.splice(userIndex, 1);
  }else{
    result.success = false;
    result.message = "User Not Found";
  }

  res.json(result)
})

module.exports = router;
