const express = require("express");
const router = express.Router();
const data = require("../db")("todo");
let idSeed = data.length;

router.get("/lists", (req, res) => {
  res.json(data);
});

router.post("/lists", (req, res)=>{
  const todoItem = req.body.value;
  data.push({
    id:`td${idSeed}`,
    value:todoItem
  });
  idSeed++;

  res.json({
    success: true
  })
})

router.patch("/lists", (req, res)=>{
  const id = req.body.id;

  let result = {
    success: true,
  };

  const target = data.find((todoItem) => {
    return todoItem.id === id;
  });

  if (target) {
    target.value = req.body.value;
    result.success = true;
  } else {
    result.success = false;
    result.message = "Todo item Not Found";
  }

  res.json(result);
})

router.delete("/lists", (req, res)=>{
  const id = req.body.id;
  let result = {
    success: true,
  };

  const itemIndex = data.findIndex((todoItem)=>{
    return todoItem.id === id;
  })

  if(itemIndex > -1){
    data.splice(itemIndex, 1);
  }else{
    result.success = false;
    result.message = "Todo item Not Found";
  }

  res.json(result)
})

module.exports = router;
