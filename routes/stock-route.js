const express = require("express");
const router = express.Router();
const data = require("../db")("stock");
const randomSeconds = 5;
const randomPrice = 20;

function updateStock(stock){
    const updateDate = stock.data.nextUpdate;
    if(updateDate.getTime() < Date.now()){
        
        stock.data.nextUpdate = new Date(Date.now() +  (Math.floor(Math.random() * randomSeconds) + randomSeconds) * 1000);
        const trend = Math.random() < 0.5 ? -1 : 1;
        stock.data.price = parseFloat((stock.data.price + (Math.random()*randomPrice*trend)).toFixed(2));
    }
}

router.get("/", (req, res)=>{
    data.forEach(stock => {
        updateStock(stock);
    }); 

    res.json(data);
})

router.get("/:id", (req, res)=>{
    const id = req.params.id;
    const target = data.find((stock)=>{
        return stock.id === id;
    })

    if(target){

        updateStock(target);
    }

    res.json(target);

})

module.exports = router;
