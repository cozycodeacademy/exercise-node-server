const express = require("express");
const router = express.Router();
const data = require("../db")("pos");
const { auth } = require("../utils/auth");

function getCategory(code, text){
    return {
        code,text
    }
}
router.get("/menu/categorys",(req, res)=>{
    res.json([
        getCategory("sashimi","Sashimi"),
        getCategory("don","Don"),
        getCategory("royal","Royal"),
        getCategory("ramen","Ramen"),
        getCategory("sushi","Sushi"),
    ]);
  
});

router.get("/menu/:cat", (req, res)=>{
    const cat = req.params.cat.toLowerCase();
    
    let menuItems = data.items;
    if(cat!=="all"){
        menuItems = data.items.filter(item=>{
            return item.cat.includes(cat)
        });
    }
   
    res.json(menuItems.map(item=>{
        return {
            id:item.id,
            name:item.name,
            price:item.price,
            img:item.img
        }
    }));
});

function generateOrder(){
    return {
        id:String(++data.orderNo).padStart(4, '0'),
        orderDateTime:new Date(),
        status:"ordering",
        items:[]
    }
}

function generateOrderItem(itemId, amount){
    return {
        itemId,amount
    }
}

router.post("/order",(req, res)=>{
    const order = generateOrder();
    data.order[order.id] = order;
    res.json(order);
});

router.post("/order/:id/check",(req, res)=>{
    const id = req.params.id;
    if(data.order[id]){
        data.order[id].status = "checking";
        res.json({success:true});
    }else{
        res.json({success:false, message:"No such order"});
    }
    
});

router.post("/order/:id/paid",auth,(req, res)=>{
    const id = req.params.id;
    if(data.order[id]){
        data.order[id].status = "paid";
        res.json({success:true});
    }else{
        res.json({success:false, message:"No such order"});
    }
});

router.post("/order/:id/item",(req, res)=>{
    let id = req.params.id;
    let {itemId, amount} = req.body;
    if(data.order[id]){
        let orderItemFound = data.order[id].items.find(orderItem=>{
            return orderItem.itemId === itemId
        });
        if(orderItemFound){
            return res.json({success:false, message:"Item has been added"})
        }        

        let foodFound = data.items.find(item=> item.id ===itemId);
        if(!foodFound){
            return res.json({success:false, message:"No such item"});
        }
        let orderItem = generateOrderItem(itemId, amount);
        data.order[id].items.push(orderItem);
        res.json({success:true, item:orderItem})
    }else{
        res.json({success:false, message:"Order doesnt exist"})
    }
});

router.patch("/order/:id/item",(req, res)=>{
    let id = req.params.id;
    let {itemId, amount} = req.body;
    if(data.order[id]){
        const target = data.order[id].items.find(item=>{
            return item.itemId===itemId;
        });

        if(target){
            target.amount = amount;
            res.json({success:true, item:target})
        }else{
            res.json({success:false, message:"Item doesnt exist"})
        }   
        
    }else{
        res.json({success:false, message:"Order doesnt exist"})
    }
});

router.delete("/order/:id/item/:itemId", (req, res)=>{
    let {id, itemId} = req.params;
    itemId = parseInt(itemId);
    if(data.order[id]){
        data.order[id].items = data.order[id].items.filter(item=>{
            return item.itemId!==itemId;
        });
        res.json({success:true})
    }else{
        res.json({success:false, message:"Order doesnt exist"})
    }
    
})

function getOrderDetail(id){
    if(!data.order[id]){
        return {};
    }

    let result = {
        ...data.order[id],
        items:data.order[id].items.map(orderItem=>{
            
            let foodItemDetail = data.items.find(foodItem =>{
                return orderItem.itemId === foodItem.id;
            })
            return {
                ...orderItem,
                name:foodItemDetail.name,
                price:foodItemDetail.price,
                img:foodItemDetail.img
            }
        })
    };
    return result;
}
router.get("/order/:id",(req, res)=>{
    let id = req.params.id;   

    res.json(getOrderDetail(id));
  
});

router.get("/order",auth, (req, res)=>{
    let orders = Object.values(data.order);
    let result = orders.map(order=>getOrderDetail(order.id))
    res.json(result);
  
});



module.exports = router;