const express = require("express");
const router = express.Router();
const { createHash } = require("crypto");
const db = require("../db")("member");
const { getJWT, auth } = require("../utils/auth");
const path = require("path");
const multer = require("multer");
const upload = multer({
    storage: multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, "./public/img/social");
        },
        filename: function (req, file, cb) {
            const uniqueSuffix = `${Date.now()}_${Math.round(
                Math.random() * 1e9
            )}`;
            const ext = path.extname(file.originalname);
            cb(null, `${uniqueSuffix}${ext}`);
        },
    }),
});

function getHash(data) {
    const hash = createHash("sha256");

    hash.update(data);
    return hash.digest("hex");
}

router.post("/register", (req, res) => {
    const id = req.body.id;
    const password = req.body.password;
    const name = req.body.name;

    if (!id || !password || !name) {
        return res.json({
            success: false,
            message: "Insufficient info",
        });
    }

    if (db.find((member) => member.id === id)) {
        return res.json({
            success: false,
            message: "Registered member",
        });
    }

    db.push({
        id: id,
        password: getHash(id + password),
        name: name,
        gender: "",
        age: -1,
        profilePicture: "",
        friendList: [],
        desc: "",
        photos: [],
    });

    return res.json({
        success: true,
        token: getJWT({ id, name }),
    });
});

router.post("/login", (req, res) => {
    const id = req.body.id;
    const password = req.body.password;

    if (!id || !password) {
        return res.json({
            success: false,
            message: "Insufficient info",
        });
    }

    const found = db.find((member) => member.id === id);

    if (!found) {
        return res.json({
            success: false,
            message: "Wrong id or password",
        });
    }

    if (found.password === getHash(id + password)) {
        return res.json({
            success: true,
            token: getJWT({ id, name: found.name }),
        });
    } else {
        return res.json({
            success: false,
            message: "Wrong id or password",
        });
    }
});

router.get("/private-data", auth, (req, res) => {
    return res.json({
        success: true,
        data: `If you miss the train I'm on, you will know that I am gone`,
    });
});

router.get("/profile", auth, (req, res) => {
    const id = req.user.id;
    let target = db.find((member) => member.id === id);
    if (target) {
        const resProfile = {
            ...target,
        };
        delete resProfile.password;

        res.json(resProfile);
    } else {
        res.json(null);
    }
});

router.get("/profile/:id", auth, (req, res) => {
    const myId = req.user.id;
    let targetId = req.params.id;
    let me = db.find((member) => member.id === myId);
    let target = db.find((member) => member.id === targetId);

    if (!target) {
        return res.json(null);
    }

    const resObject = { ...target };
    delete resObject.password;

    if (!me.friendList.includes(targetId)) {
        delete resObject.photos;
        delete resObject.friendList;
    }
    res.json(resObject);
});

router.patch("/profile", auth, (req, res) => {
    const { id } = req.user;
    const { gender, age, desc } = req.body;

    let target = db.find((member) => member.id === id);

    let genderLowerCase = gender.toLowerCase();

    target.gender =
        ["m", "f", "o"].indexOf(genderLowerCase) > -1 ? genderLowerCase : "";
    target.age = parseInt(age);
    target.desc = desc;

    res.json({
        success: true,
    });
});

router.patch(
    "/profile/avatar",
    auth,
    (req, res, next) => {
        upload.single("avatar")(req, res, (err) => {
            if (err) {
                console.log(err);
                return res.json({
                    success: false,
                    message:
                        "File upload error. Please check the file you uploaded.(max 1)",
                });
            }
            if (!req.file) {
                return res.json({
                    success: false,
                    message: "No picture provided",
                });
            }
            next();
        });
    },
    (req, res) => {
        const { id } = req.user;
        const { destination, filename } = req.file;

        let target = db.find((member) => member.id === id);
        target.profilePicture = `${destination}/${filename}`.replace(
            "./public",
            ""
        );
        res.json({
            success: true,
            path: target.profilePicture,
        });
    }
);

router.post(
    "/profile/photos",
    auth,
    (req, res, next) => {
        upload.array("photos", 3)(req, res, (err) => {
            if (err) {
                return res.json({
                    success: false,
                    message:
                        "File upload error. Please check the files you uploaded.(max 3)",
                });
            }
            if (!req.files) {
                return res.json({
                    success: false,
                    message: "No picture provided",
                });
            }
            next();
        });
    },
    (req, res) => {
        const { id } = req.user;
        let target = db.find((member) => member.id === id);

        let paths = [];
        req.files.forEach((file) => {
            const { destination, filename } = file;
            const path = `${destination}/${filename}`.replace("./public", "");
            target.photos.push(path);
            paths.push(path);
        });

        res.json({
            success: true,
            paths,
        });
    }
);

module.exports = router;
