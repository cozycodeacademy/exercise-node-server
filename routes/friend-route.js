const express = require("express");
const router = express.Router();

const friendRequestDB = require("../db")("friendRequest");
const memberDB = require("../db")("member");
const { auth } = require("../utils/auth");

// query? gender, minAge, maxAge
router.get("/search", auth, (req, res) => {
  const userId = req.user.id;
  const userProfile = memberDB.find(user => user.id === userId);
  const userFriendList = userProfile.friendList;

  let { gender, minAge, maxAge } = req.query;
  if (!gender) {
    gender = "f";
  }
  if (!minAge) {
    minAge = 16;
  }
  if (!maxAge) {
    maxAge = 40;
  }
  let targets = memberDB.filter((member) => {
    return (
      member.age >= minAge &&
      member.age <= maxAge && 
      member.gender === gender &&
      userFriendList.indexOf(member.id) <0 &&
      member.id !== userId
    );
  });
  if(targets.length === 0){
    return res.json(null);
  }
  const rand = Math.floor(Math.random() * targets.length);
  const target = {...targets[rand]};
  delete target.friendList;
  delete target.photos;
  delete target.password;
  res.json(target);
});

// no body
router.post("/requests", auth, (req, res) => {
  const { id } = req.user;
  const targetId = req.body.memberId;
  const me = memberDB.find((member) => member.id === id);
  const isFriend = me.friendList.includes(targetId);

  if (targetId === id || isFriend) {
    return res.json({
      success: true,
      message: "friend already",
    });
  }

  const targetExisted = !!memberDB.find((member) => member.id === targetId);
  if(!targetExisted){
    return res.json({
      success: false,
      message:"No such user"
    });
  }
  
  const requestExisted = !!friendRequestDB.find(
    (fdReq) => fdReq.sender === id && fdReq.receiver === targetId
  );

  if (!requestExisted) {
    friendRequestDB.push({
      sender: id,
      receiver: targetId,
      time: new Date(),
    });
  }

  res.json({
    success: true,
  });
});

// no body
router.post("/requests/accept", auth, (req, res) => {
  const { id } = req.user;
  const targetId = req.body.memberId;

  const friendRequestIndex = friendRequestDB.findIndex(
    (fdReq) => fdReq.sender === targetId && fdReq.receiver === id
  );
  if (friendRequestIndex>-1) {
    const me = memberDB.find((member) => member.id === id);
    if(me.friendList.indexOf(targetId)===-1){
        me.friendList.push(targetId);
    }

    const target = memberDB.find((member) => member.id === targetId);
    if(target.friendList.indexOf(id)===-1){
        target.friendList.push(id);
    }
    friendRequestDB.splice(friendRequestIndex,1);
    res.json({
      success: true,
    });
  } else {
    res.json({
      success: false,
      message: "No such friend request record",
    });
  }
});

router.delete("/requests", auth, (req, res) => {
  const { id } = req.user;
  const targetId = req.body.memberId;

  const friendRequestIndex = friendRequestDB.findIndex(
    (fdReq) => fdReq.sender === targetId && fdReq.receiver === id
  );
  if (friendRequestIndex>-1) {
    friendRequestDB.splice(friendRequestIndex,1);
    res.json({
      success: true,
    });
  } else {
    res.json({
      success: false,
      message: "No such friend request record",
    });
  }
});

router.get("/requests", auth, (req, res)=>{
    const { id } = req.user;
    let allRequest = friendRequestDB.filter(fdReq=>fdReq.receiver===id);
    let result = allRequest.map(fdReq=>{
      const senderId = fdReq.sender;
      let { id, name, gender, age, profilePicture } = memberDB.find(member=>{
        return member.id === senderId;
      });
      return {
        id, name, gender, age, profilePicture,
        time:fdReq.time
      }
    })
    res.json(result);
})

module.exports = router;
