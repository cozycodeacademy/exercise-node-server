const express = require("express");
const app = express();
const userRoute = require("./routes/user-route");
const todoRoute = require("./routes/todo-route");
const stockRoute = require("./routes/stock-route");
const memberRoute = require("./routes/member-route");
const videoRoute = require("./routes/video-route");
const posRoute = require("./routes/pos-route");
const friendRoute = require("./routes/friend-route");
const qaRoute = require("./routes/qa-route");

app.use(express.static('public'));
app.use(express.json());
app.use("/",userRoute);
app.use("/todo",todoRoute);
app.use("/stock",stockRoute);
app.use("/member",memberRoute);
app.use("/video", videoRoute);
app.use("/pos", posRoute);
app.use("/friend", friendRoute);
app.use("/qa", qaRoute);

app.use((req, res)=>{
  res.status(404).json({
    success: false,
    message:"404 not found"
  })
});

app.listen(3000, () => {
  console.log("Web server is running on port 3000!");
});
