const jwt = require("jsonwebtoken");
const jwtSecret = `cozycode${Date.now()}`;

module.exports.getJWT = (data)=> {
    return jwt.sign(
        data,
        jwtSecret
    );
}

module.exports.auth = (req, res, next) =>{
  const authHeader = req.headers.authorization;
  if(!authHeader){
    return res.status(401).json({
      success: false,
      message: "Please provide access token"
    })
  }
  
  const token = authHeader.split("Bearer ")[1];
  jwt.verify(token, jwtSecret, (err, decoded)=>{
    if(err){
      res.status(401).json({
        success: false,
        message: "token invalid"
      })
    }else{
        req.user = decoded;
        next();
    }
  })
  
}
