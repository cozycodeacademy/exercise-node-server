const videoSource = require("./utils/video-source");

const data = {
    user:[
        {
            "name": "Terry",
            "gender": "M"
        },
        {
            "name": "William",
            "gender": "M"
        },
        {
            "name": "John",
            "gender": "M"
        },
        {
            "name": "FuFu",
            "gender": "F"
        }
    ],
    todo:[
        {
            id:"td0",
            value:"homework"
        }
    ],
    stock:[
        {
            id:"MS",
            data:{
                nextUpdate: new Date(),
                price: 100
            }
        },
        {
            id:"APPLE",
            data:{
                nextUpdate: new Date(),
                price: 180
            }
        }
    ],
    member:[
      {
          id:"fakekit",
          password:"baa011a9f12989431573c0e41eb3e54e3ca9e78133e66a1312c596ccbc1ca491",
          name:"Kit",
          gender:"m",
          age:18,
          profilePicture:"/img/social/m10.jpg",
          friendList:[],
          desc:"A rubber band will only get looser the more you stretch it, just like human morality, it can’t go back",
          photos:["/img/social/m1.jpg","/img/social/m2.jpg"]
      },
      {
          id:"faketony",
          password:"b8e0abc90d7a7ac6e79183fa0b15605672f4c654e880510315188d92f84c22a5",
          name:"Tony",
          gender:"m",
          age:19,
          profilePicture:"/img/social/m5.jpg",
          friendList:[],
          desc:"Light a cigarette and drink a sip of wine, all the unhappiness will flow away.",
          photos:["/img/social/m7.jpg","/img/social/m8.jpg"]
      },
      {
          id:"fakeken",
          password:"807eb05f4ee11e3a5cb4ad2a1f12e61a9fc4d728c8b5fba23d9366718eb26b18",
          name:"Ken",
          gender:"m",
          age:18,
          profilePicture:"/img/social/m11.jpg",
          friendList:[],
          desc:"You can test gold with fire, test women with gold, test men with women, but I only love sports.",
          photos:["/img/social/m11.jpg","/img/social/m14.jpg"]
      },
      {
          id:"fakenana",
          password:"0c59921c38fe6d8d7171a9c8cccd8629095dbf4f158d97f4f9f242cb9f6d90ea",
          name:"Nana",
          gender:"f",
          age:22,
          profilePicture:"/img/social/f13.jpg",
          friendList:[],
          desc:"I want to make friends sincerely, if you are not interested in buying weight loss pills, please don’t bother me.",
          photos:["/img/social/f14.jpeg","/img/social/f15.webp","/img/social/f16.jpeg"]
      },
      {
          id:"fakefafa",
          password:"6d63b3abe3528058298da5b77f17ef776c406693370579f4f2d3372b790d7010",
          name:"FaFa",
          gender:"f",
          age:16,
          profilePicture:"/img/social/f3.jpg",
          friendList:[],
          desc:"Everyone will take one great picture, I’ve done better because I’ve taken two.",
          photos:["/img/social/f17.jpeg","/img/social/f18.jpeg","/img/social/f19.jpeg"]
      },
      {
          id:"fakemay",
          password:"5c2d38f929752f17b8e7eb6f94813abf3f98b7d651dac5abcfcf1ff7c875ed0d",
          name:"May May",
          gender:"f",
          age:17,
          profilePicture:"/img/social/f10.jpg",
          friendList:[],
          desc:"A man who truly loves you, even if you scold him, hit him, or ask for a breakup, he will not leave you.",
          photos:["/img/social/f1.jpg","/img/social/f8.jpg"]
      }
  ],
    video:videoSource(20),
    pos:{
        items:[
            {
                id:0,
                name:"Tuna Sashimi",
                price:200,
                img:"/img/pos/bluefin.jpg",
                cat:["sashimi","royal"]
            },
            {
                id:1,
                name:"Hiramasa Sashimi",
                price:12,
                img:"/img/pos/bullyingfish.jpg",
                cat:["sashimi"]
            },
            {
                id:2,
                name:"Koi Sashimi",
                price:120,
                img:"/img/pos/chifish.jpg",
                cat:["sashimi"]
            },
            {
                id:3,
                name:"Sweet Shrimp Sashimi",
                price:36,
                img:"/img/pos/prawn.jpg",
                cat:["sashimi"]
            },
            {
                id:4,
                name:"Scallop Sashimi",
                price:78,
                img:"/img/pos/scallop.jpg",
                cat:["sashimi"]
            },
            {
                id:5,
                name:"Chinese Sea Urchin",
                price:880,
                img:"/img/pos/seaurchins.jfif",
                cat:["sashimi"]
            },
            {
                id:6,
                name:"Luxury Sashimi Platter",
                price:560,
                img:"/img/pos/set.jpg",
                cat:["sashimi","royal"]
            },
            {
                id:7,
                name:"Affordable Sashimi Platter",
                price:140,
                img:"/img/pos/set2.jpg",
                cat:["sashimi"]
            },
            {
                id:8,
                name:"Beef Bowel Rice Bowl",
                price:100,
                img:"/img/pos/don2.jpg",
                cat:["don"]
            },
            {
                id:9,
                name:"Noble Bowel Rice Bowl",
                price:240,
                img:"/img/pos/don.jpg",
                cat:["don", "sashimi","royal"]
            },
            {
                id:10,
                name:"Regular Ramen",
                price:100,
                img:"/img/pos/ramen.jpg",
                cat:["ramen"]
            },
            {
                id:11,
                name:"Ordinary Ramen",
                price:90,
                img:"/img/pos/ramen2.jpg",
                cat:["ramen"]
            },
            {
                id:12,
                name:"Royal Sushi Platter",
                price:670,
                img:"/img/pos/sushi.jpg",
                cat:["sushi","royal"]
            },
            {
                id:13,
                name:"Toro Sushi",
                price:560,
                img:"/img/pos/sushi2.jpg",
                cat:["sushi","royal"]
            },
            {
                id:14,
                name:"Omakase",
                price:1200,
                img:"/img/pos/sushi3.jpg",
                cat:["sushi","royal"]
            },
        ],
        order:{

        },
        orderNo:0
    },
    friendRequest:[],
    qa: {
        m: [
          {
            id:0,
            q: "AA制定男仔俾?",
            a: [
              { a: "AA", point: 10 },
              { a: "節日男仔俾, 平時AA", point: 5 },
              { a: "緊係男仔俾", point: -1 },
            ],
          },
          {
            id:1,
            q: "男仔在打機時可以唔接聽女朋友電話嗎?",
            a: [
              { a: "可以", point: 10 },
              { a: "最好唔好", point: 5 },
              { a: "馬上熄game, 一定要聽", point: -1 },
            ],
          },
          {
            id:2,
            q: "我有一層樓, 唔駛你供, 唔寫你名得唔得?",
            a: [
              { a: "可以", point: 5 },
              { a: "一齊供, 唔駛寫我名", point: 10 },
              { a: "唔得, 最好俾埋家用我", point: -1 },
            ],
          },
          {
            id:3,
            q: "男仔每個月可以使幾多錢落玩具度?",
            a: [
              { a: "隨你", point: 5 },
              { a: "使錢喺我度就得", point: -1 },
              { a: "5000", point: 10 },
            ],
          },
          {
            id:11,
            q:"試金可以用火, 試女人可以用金, 試男人可以用咩?",
            a:[
                {a:"女人",point:-1},
                {a:"真誠",point:10},
                {a:"Game",point:5}
            ]
          }
        ],
        f: [
          {
            id:4,
            q: "你為左愛情可以有幾上進?",
            a: [
              { a: "我爸爸係上市公所大股東", point: 10 },
              { a: "瘋狂OT, 擦上司鞋, 升唔到職唔返屋企", point: 5 },
              { a: "去努力進修, 堅毅無比", point: -1 },
            ],
          },
          {
            id:5,
            q: "男主外, 女主內, 你同意嗎?",
            a: [
              { a: "同意, 結婚後我養你, 隨時可以退休做少奶", point: 10 },
              { a: "唔同意, 男女平等", point: 5 },
              { a: "唔同意, 求你包養我", point: -1 },
            ],
          },
          {
            id:6,
            q: "每年你會帶我去幾多次旅行?",
            a: [
              { a: "兩次", point: 5 },
              { a: "俾得起都可以帶你去", point: 10 },
              { a: "一齊做backpacker流浪", point: -1 },
            ],
          },
          {
            id:7,
            q: "我可以落bar玩嗎?",
            a: [
              { a: "一個星期一次好啦", point: 5 },
              { a: "可以, 我都會落", point: -1 },
              { a: "呢個係你嘅權利, 我無意見", point: 10 },
            ],
          },
          {
            id:8,
            q:"如果你真係愛一個女仔, 即使個女仔鬧你、打你, 你會離開佢嗎?",
            a:[
                {a:"會, 女人應該要溫柔",point:-1},
                {a:"我超抖M",point:5},
                {a:"唔離開不特止仲會分一半身家俾佢",point:10}
            ]
          },
          {
            id:9,
            q:"試金可以用火, 試男人可以用女人, 試女人可以用咩?",
            a:[
                {a:"女人唔係用來試, 唔好物化女仔",point:10},
                {a:"金",point:-1},
                {a:"男人",point:5}
            ]
          },
          {
            id:10,
            q:"我可摸耳著瑜珈褲出街?",
            a:[
                {a:"我買條儒家褲俾你",point:-1},
                {a:"可以, 但只可以當底褲著",point:5},
                {a:"你是恆久的美麗, 任何衣著都襯托出你嘅魅力",point:10}
            ]
          }
        ]
    }

};


module.exports = function(store){
    return data[store]? data[store]: [];
};